# Case Studies

NERSC staff along with vendor engineers have worked with
[NESAP](https://www.nersc.gov/research-and-development/nesap/)
applications to optimize codes for the Cori and Perlmutter
architectures. Several of these efforts are documenated as case
studies.

## GPU Case Studies

* [AMReX GPU Case Study](amrex-gpu/index.md)
* [DESI GPU Case Study](desi/index.md)
* [MetaHipMer GPU Case Study](metahipmer/index.md)
* [Tomopy GPU Case Study](tomopy/index.md)
* [SNAP GPU Case Study](snap/index.md)
* [C++ ML Interface](CPP_2_Py/index.md)

## KNL Case Studies

* [AMReX KNL Case Study](amrex/index.md)
* [BerkeleyGW KNL Case Study](berkeleygw/index.md)
* [Chombo-Crunch KNL Case Study](chombo-crunch/index.md)
* [EMGeo KNL Case Study](emgeo/index.md)
* [HMMER3 KNL Case Study](hmmer3/index.md)
* [MFDn KNL Case Study](mfdn/index.md)
* [QPhiX KNL Case Study](qphix/index.md)
* [Quantum ESPRESSO KNL Case Study](quantum-espresso/index.md)
* [WARP KNL Case Study](warp/index.md)
* [XGC1 KNL Case Study](xgc1/index.md)
