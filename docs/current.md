# Current known issues

## Perlmutter

!!! warning "Perlmutter is not a production resource"
    Perlmutter is not a production resource and usage is not charged
    against your allocation of time. While we will attempt to make the
    system available to users as much as possible, it is subject to
    unannounced and unexpected outages, reconfigurations, and periods
    of restricted access. Please visit the [timeline
    page](systems/perlmutter/timeline/index.md) for more information
    about changes we've made in our recent upgrades.

NERSC has automated monitoring that tracks failed nodes, so please
only open tickets for node failures if the node consistently has poor
performance relative to other nodes in the job or if the node
repeatedly causes your jobs to fail.

### New issues 

- Building software fails with message like `cannot find -lcublas` in PrgEnv-gnu. This is because 
  the cublas (and other CUDA math) libraries are in a different location to the 
  main cudatoolkit libraries, and the modulefiles are missing the environment 
  variables that would enable the compiler and linker to find them. You can work 
  around this by setting some environment variables after loading cudatoolkit:
  
   ```
   perlmutter$ export LIBRARY_PATH="${LIBRARY_PATH}:${CUDATOOLKIT_HOME}/../../math_libs/lib64"
   perlmutter$ export CPATH="${CPATH}:${CUDATOOLKIT_HOME}/../../math_libs/include"
   ```

### Ongoing issues

- The default CPU architecture module is `craype-x86-milan`, which corresponds
  to the CPU architecture on Perlmutter login and compute nodes. However, users
  may see compiler errors in some circumstances:
    - Currently the Cray Wrappers in PrgEnv-nvidia are hardcoded to optimize code for AMD Rome (zen2),
      to generate code optimized for AMD Milan you must pass `-march=znver3` or `-tp=zen3`
      to your compilation step.
- MPI users may hit `segmentation fault` errors when trying
  to launch an MPI job with many ranks due to incorrect
  allocation of GPU memory. We provide [more information
  and a suggested workaround](systems/perlmutter/index.md#known-issues-with-cuda-aware-mpi).
- Some users may see messages like `-bash:
  /usr/common/usg/bin/nersc_host: No such file or directory` when you
  login. This means you have outdated dotfiles that need to be
  updated. To stop this message, you can either delete this line from
  your dot files or check if `NERSC_HOST` is set before overwriting
  it. Please see our [environment
  page](environment/index.md#home-directories-shells-and-dotfiles)
  for more details.
- [Known issues for Machine Learning applications](machinelearning/known_issues.md)

!!! caution "Be careful with NVIDIA Unified Memory to avoid crashing nodes"
    In your code, [NVIDIA Unified Memory](https://developer.nvidia.com/blog/unified-memory-cuda-beginners/)
    might
    look something like `cudaMallocManaged`. At the moment, we do not have
    the ability to control this kind of memory and keep it under a safe
    limit. Users who allocate a large
    pool of this kind of memory may end up crashing nodes if the UVM
    memory does not leave enough room for necessary system tools like
    our filesystem client.
    We expect a fix in early 2022. In the meantime, please keep the size
    of memory pools allocated via UVM relatively small. If you have
    questions about this, please contact us.

## Cori

The Burst Buffer on Cori has a number of known issues, documented at [Cori Burst Buffer](filesystems/cori-burst-buffer.md#known-issues).
