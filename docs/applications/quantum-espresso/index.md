# Quantum ESPRESSO/PWSCF

[Quantum ESPRESSO](https://www.quantum-espresso.org) is an integrated
suite of computer codes for electronic structure calculations and
materials modeling at the nanoscale. It builds on the electronic
structure codes PWscf, PHONON, CP90, FPMD, and Wannier.  It is based
on density-functional theory, plane waves, and pseudopotentials (both
norm-conserving and ultrasoft).

## Availability and Supported Architectures at NERSC

Quantum ESPRESSO is available at NERSC as a [provided support level](../../policies/software-policy/index.md)
package. Quantum Espresso 7.X supports GPU execution.

### Versions Supported

| Cori Haswell  | Cori KNL  | Perlmutter GPU  | Perlmutter CPU (Anticipated)  |
|---------------|-----------|-----------------|-------------------------------|
| 7.X           | 7.X       | 7.X             | (7.X)                         |

## Application Information, Documentation, and Support

Quantum ESPRESSO is freely available and can be downloaded from the [Quantum ESPRESSO home page](https://www.quantum-espresso.org).
See the preceding link for more resources,
including documentation for building the code and preparing input files,
tutorials, pseudopotentials, and auxiliary software.
For troubleshooting, see the [FAQ](https://www.quantum-espresso.org/faq/)
for solutions to common issues encountered while building and running the package.
See the Quantum Espresso
[users forum](https://www.quantum-espresso.org/users-forum/) and
[mail archives](https://www.mail-archive.com/users@lists.quantum-espresso.org/)
for additional support-related questions.
For help with issues specific to the NERSC module, please file a
[support ticket](https://help.nersc.gov).

## Using Quantum ESPRESSO at NERSC

Use the `module avail` command to see which versions are available and
`module load espresso/<version>` to load the environment:

```console
nersc$ module avail espresso
   espresso/7.0-libxc-5.2.2

nersc$ module load espresso/7.0-libxc-5.2.2
```

The preceding command loads Quantum ESPRESSO 7.0 built with the LIBXC 5.2.2
density functional library.

## Sample Job Scripts

See the [example jobs page](../../jobs/examples/index.md) for additional
examples and information about jobs. For all routines except `pw.x`, run
Quantum ESPRESSO in full MPI mode as there is currently no efficient OpenMP
implementation available.

??? example "Cori Haswell"

	```bash
	#!/bin/bash
	#SBATCH --qos=regular
	#SBATCH --nodes=2
	#SBATCH --ntasks-per-node=32
	#SBATCH -C haswell
	#SBATCH -t 02:00:00
	#SBATCH -J my_job

	export OMP_NUM_THREADS=1

	module load espresso/7.0-libxc-5.2.2
	srun ph.x -input test.in
	```

	!!! warning
		Pay close attention to the explicit setting of
		`OMP_NUM_THREADS=1` when running in pure MPI mode. This is optimal
		when intending to run with only MPI tasks.

### Hybrid DFT

We have optimized the hybrid DFT calculations in Quantum ESPRESSO
(`pw.x`). These changes are described in our
[Quantum ESPRESSO case study](../../performance/case-studies/quantum-espresso/index.md).

The following scripts provides the best `pw.x` performance for hybrid
functional calculations:

??? example "Cori Haswell"

	```slurm
	#!/bin/bash
	#SBATCH --qos=regular
	#SBATCH --nodes=2
	#SBATCH --ntasks-per-node=4
	#SBATCH --cpus-per-task=16
	#SBATCH -C haswell
	#SBATCH -t 02:00:00
	#SBATCH -J my_job

	export OMP_NUM_THREADS=8
	export OMP_PLACES=threads
	export OMP_PROC_BIND=spread

	module load espresso/7.0-libxc-5.2.2
	srun --cpu-bind=cores pw.x -nbgrp 8 -input test.in
	```

??? example "Cori KNL"

	```slurm
	#!/bin/bash
	#SBATCH --qos=regular
	#SBATCH --nodes=2
	#SBATCH --ntasks-per-node=4
	#SBATCH --cpus-per-task=68
	#SBATCH -C knl,quad,cache
	#SBATCH -t 02:00:00
	#SBATCH -J my_job

	export OMP_NUM_THREADS=16
	export OMP_PLACES=threads
	export OMP_PROC_BIND=spread

	module load espresso/7.0-libxc-5.2.2
	srun --cpu-bind=cores pw.x -nbgrp 8 -input test.in
	```

!!! tip
	For band-group parallelization, it is recommended to run one
	band group per MPI rank. However, please keep in mind that it is
	not possible to use more band-groups than there are bands in your
	system, so adjust the number accordingly if issues are
	encountered.

!!! note
	The new implementation is much more efficient, so you might
	be able to use much fewer nodes and still get the solution within
	the same wallclock time.

### GPU calculations on Perlmutter

??? example "Perlmutter"

	```slurm
	#!/bin/bash
	#SBATCH --qos=regular
	#SBATCH --nodes=2
	#SBATCH --ntasks-per-node=4
	#SBATCH --gpus-per-task=1
	#SBATCH --gpu-bind=map_gpu:0,1,2,3
	#SBATCH -C gpu
	#SBATCH -t 02:00:00
	#SBATCH -J my_job

	export SLURM_CPU_BIND="cores"
	export OMP_PROC_BIND=true
	export OMP_PLACES=threads
	export OMP_NUM_THREADS=1

	module load espresso/7.0-libxc-5.2.2
	srun ph.x -input test.in
	```

## Building Quantum ESPRESSO from Source

Some users may be interested in tweaking the Quantum ESPRESSO build
parameters and building QE themselves. Quantum Espresso tarballs are
available for download at the developers'
[download page](https://www.quantum-espresso.org/login/).
Our build instructions for the QE module are listed below. 

!!! note
	Not all versions are available for all architectures or
	compilers.

??? example "Building on Cori"

	The following procedure was used to build
	Quantum ESPRESSO versions >5.4 on Cori. In the root QE directory:

	```bash
	cori$ ./configure
	cori$ cp /usr/common/software/espresso/<version>/<arch>/<comp>/make.inc .
	cori$ make <application-name, e.g. pw>
	```

	where `<version>` specifies the version, `<arch>` the architecture
	(usually `hsw` or `knl` for Haswell and KNL respectively) and `<comp>`
	the compiler (usually `gnu` or `intel`).

??? example "Building on Perlmutter"

	To build Quantum ESPRESSO 7.0 on Perlmutter, navigate to
	the main `qe-7.0` directory and execute the following commands:

	```bash
	perlmutter$ export LC ALL=C
	perlmutter$ module load PrgEnv-nvidia ; module load cray-fftw/3.3.8.12 ; module load cudatoolkit/11.0
	perlmutter$ ./configure CC=cc CXX=CC FC=ftn MPIF90=ftn --with-cuda=$CUDA_HOME --with-cuda-cc=80 --with-cuda-runtime=11.0 --enable-openmp --with-scalapack=no FFLAGS="-Mpreprocess" FCFLAGS="-Mpreprocess" LDFLAGS="-acc"
	perlmutter$ make all
	```

	To link to a pre-existing LibXC build, add the following options to the
        `./configure` command above:

	```bash
	--with-libxc=yes --with-libxc-prefix=<path-to-your-libxc-install-directory>
	```
	!!! note
		LIBXC must be built using the same Fortran compiler as Quantum
		ESPRESSO, or the `./configure` script will fail to find the LIBXC library.

!!! tip
	To run the included examples one may need to modify the prefix and directory
	paths in the file `environment_variables` in the main QE directory.

## Related Applications

* [Abinit](../abinit/index.md)
* [CP2K](../cp2k/index.md)
* [JDFTx](https://jdftx.org/)
* [Octopus](https://www.octopus-code.org/wiki/Main_Page)
* [PARSEC](http://real-space.org/)
* [RMGDFT](https://github.com/RMGDFT/rmgdft)
* [SIESTA](../siesta/index.md)
* [VASP](../vasp/index.md)

## User Contributed Information

!!! info "Please help us improve this page"
        Users are invited to contribute helpful information and corrections
        through our [GitLab repository](https://gitlab.com/NERSC/nersc.gitlab.io/blob/main/CONTRIBUTING.md).
