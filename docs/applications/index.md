# Applications

NERSC provides a wide variety of pre-built applications, optimized for
our systems. The primary way these are provided in the [NERSC
environment](../environment/index.md) is through
[modules](../environment/modules.md).

!!! tip "Best practices"
    [Best practices](../jobs/best-practices.md) are not specific to any
    one application and ensure efficient use your project's allocation.

!!! note
    Some available applications may be present, but may not
    necessarily have a dedicated page on this site. Please consult the
    `module avail` command on a NERSC system to see what is available.

In addition to application specific details NERSC also provides:

* additional [example jobs](../jobs/examples/index.md)
* [workflow tools](../jobs/workflow-tools.md) for coordination/ automation.
* A [guide](../getting-started.md) for new users.

## Popular applications

The tables here summarize the latest version available at NERSC, for
different architectures, of several popular applications. **Note** that
the Perlmutter software stack is still being built out: very few applications
are available at this time. For Perlmutter, these tables indicate
applications that we anticipate becoming available during the
early-access period.

### Density functional theory

| Application                                   | Haswell    | KNL        | Perlmutter GPU (Anticipated)  |
|-----------------------------------------------|------------|------------|-------------------------------|
| [BerkeleyGW](berkeleygw/index.md)             | 2.X        | 2.X        | 3.X                           |
| [CP2K](cp2k/index.md)                         | 6.1        | 6.1        | Yes                           |
| [SIESTA](siesta/index.md)                     | 4.1.5      | 4.1.5      | -                             |
| [Quantum ESPRESSO](quantum-espresso/index.md) | 7.X        | 7.X        | 7.X                           |
| [VASP](vasp/index.md)                         | 20181030   | 20181030   | Yes (6.2.1)                   |
| [Wannier90](wannier90/index.md)               | 3.0.0      | 3.0.0      | -                             |

### Molecular dynamics

| Application                                   | Haswell    | KNL        | Perlmutter GPU (Anticipated) |
|-----------------------------------------------|------------|------------|------------------------------|
| [AMBER](amber/index.md)                       | 20         | -          | Yes                          |
| [Abinit](abinit/index.md)                     | 8.10.3     | 8.10.3     | (see below)                  |
| [Gromacs](gromacs/index.md)                   | 2020.2     | 2020.2     | -                            |
| [LAMMPS](lammps/index.md)                     | 2021.04.23 | 2021.04.23 | Yes                          |
| [NAMD](namd/index.md)                         | 2.1.3      | 2.13       | -                            |

### Chemistry applications

| Application                                   | Haswell    | KNL        | Perlmutter GPU (Anticipated) |
|-----------------------------------------------|------------|------------|------------------------------|
| [GAMESS](gamess/index.md)                     | 30SEPT2019 | -          | (see below)                  |
| [MOLPRO](molpro/index.md)                     | 2015.1     | -          | -                            |
| [Q-Chem](qchem/index.md)                      | 5.2.2      | -          | (see below)                  |
| [NWChem](nwchem/index.md)                     | 7.0.0      | 7.0.0      | Yes                          |

### Mathematical environments

| Application                                   | Haswell    | KNL        | Perlmutter GPU (Anticipated) |
|-----------------------------------------------|------------|------------|------------------------------|
| [Mathematica](mathematica/index.md)           | 12.0.0     | -          | Yes (12.3.1)                 |
| [MATLAB](matlab/index.md)                     | R2020b     | -          | Yes (R2021a)                 |

### Visualization

| Application                                   | Haswell    | KNL        | Perlmutter GPU (Anticipated) |
|-----------------------------------------------|------------|------------|------------------------------|
| [VisIt](visit/index.md)                       | 3.1.2      | -          | -                            |
| [ParaView](paraview/index.md)                 | 5.6.0      | -          | Yes (5.9.1)                  |
