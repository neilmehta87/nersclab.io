# CP2k

CP2K is a quantum chemistry and solid state physics software package
that can perform atomistic simulations of solid state, liquid,
molecular, periodic, material, crystal, and biological systems.

## Support

The [CP2k Reference Manual](https://manual.cp2k.org/) provides details
on how to setup calculations and the various options available.

For questions about cp2k usage that are not specific to NERSC please
consult the [CP2k Forum](https://groups.google.com/group/cp2k) and
[CP2k FAQ](https://www.cp2k.org/faq).

If you need to make your own customized build of CP2k the [Makefile
and build
script](https://gitlab.com/NERSC/nersc-user-software/-/tree/master/manual-build-scripts/cp2k)
used to create NERSC's modules are available.

!!! tip
	If *after* consulting with the above you believe there is an
	issue with the NERSC module, please file
	a [support ticket](https://help.nersc.gov).
	
## CP2k at NERSC

NERSC provides modules for [cp2k](https://www.cp2k.org).

Use the `module avail` command to see what versions are available:

```bash
module avail cp2k
```

### Perlmutter

!!! note 
	NERSC does not currently provide a GPU enabled module of cp2k
	for Perlmutter, but it being actively investigated.

### Example - Cori KNL

```slurm
#!/bin/bash
#SBATCH --qos=regular
#SBATCH --constraint=knl
#SBATCH --time=300
#SBATCH --nodes=4
#SBATCH --ntasks-per-node=64
#SBATCH --cpus-per-task=4
#SBATCH --core-spec=2

module unload craype-haswell
module load craype-mic-knl
module load cp2k/6.1

srun --cpu-bind=cores cp2k.popt -i example.inp
```

### Example - Cori KNL with OpenMP

```slurm
#!/bin/bash
#SBATCH --qos=regular
#SBATCH --constraint=knl
#SBATCH --time=300
#SBATCH --nodes=4
#SBATCH --ntasks-per-node=32
#SBATCH --cpus-per-task=8
#SBATCH --core-spec=2

module unload craype-haswell
module load craype-mic-knl
module load cp2k/6.1
export OMP_NUM_THREADS=2

srun --cpu-bind=cores cp2k.psmp -i example.inp
```

### Example - Cori Haswell

```slurm
#!/bin/bash
#SBATCH --qos=regular
#SBATCH --constraint=haswell
#SBATCH --time=300
#SBATCH --nodes=4
#SBATCH --ntasks-per-node=32
#SBATCH --cpus-per-task=2

module load cp2k/6.1

srun --cpu-bind=cores cp2k.popt -i example.inp
```

### Example - Cori Haswell with OpenMP

```slurm
#!/bin/bash
#SBATCH --qos=regular
#SBATCH --constraint=haswell
#SBATCH --time=300
#SBATCH --nodes=4
#SBATCH --ntasks-per-node=16
#SBATCH --cpus-per-task=4

module load cp2k/6.1
export OMP_NUM_THREADS=2

srun --cpu-bind=cores cp2k.popt -i example.inp
```

## Performance

Performance of cp2k can vary depending on the system size and run type. The 
multinode scaling performance of the code depends on the amount of work (or 
number of atoms) per MPI rank. It is recommended to try a representative 
test case on different number of nodes to see what gives the best performance.
